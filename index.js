console.log("Hello World!");

// // /*
// // -Create a function that will accept the name of the user
// // and will print each character of the name in reverse order.
// // -If the lengthof the name is more than seven characters, the consonants of the name wi
// // of the name will only be printedin the console.
// // */

// // let userName = prompt("Enter your Name: ");


// // function reverseName(userName){
  
// //  for(let i = userName.length-1; i >=0; i--){
// //    if(userName.length > 7){
// //       if(userName[i] == "a" || userName[i] == "e" ||userName[i] == "i" ||userName[i] == "o" ||userName[i] == "u" ){
// //         continue;
// //       }
// //        console.log(userName[i]);
// //      }
// //    else{
// //      console.log(userName[i]);
// //    }
// //  }
 
// // }

// // reverseName(userName);



// let students = ['Jane', 'John', 'Joe'];

// // console.log(students);

// // // 1. Total size of an Array

// console.log(students.length);

// // // 2. Adding a new element at the beginning of an Array.(Add Jack)

// students.unshift('Jack');
// console.log(students);

// // // 3. Adding a new element at the end of an array.

// students.push('Jill');
// console.log(students);

// // // 4. Removing an existing element at the end of an array.

// students.shift('Jack');
// console.log(students);

// // // 5.) Removing an existing element at the end of an array.

// students.pop('Jill');
// console.log(students);

// // // 6.) Find the index number of the last element in an array.

// lastIndex = students.length-1
// console.log(students[lastIndex]);

// // 7.) Print each element of the array in the browser console.

// console.log(students);

// /*
// Using the students array, create a function that will look for the 
// index of a given student and will remove the name of the student from the array list.
// */

let names = ['John', 'Jane', 'Joe'];

// get input name
let usrName = prompt("Enter your Name: ");

const userIndexToDel = names.indexOf(usrName);

names.splice(userIndexToDel,1);
if (userIndexToDel > -1) {
  names.splice(userIndexToDel,1);
  console.log(names);
}
else {
  console.log('User does not exist.')
}

/*
Expected output:
//Initial content of arrays:
[John, Jane, Joe]
*/


/*
//After finding the index and removing the student "Jane"
[John, Joe]
To enable screen reader support, press ⌘+Option+Z To learn about keyboard shortcuts, press ⌘slash
 */

//6. Create a "person" object that contains properties first name, last name, nickname, age, address(contains city and country), 
// friends(with at least 3 friends with the first name), and a function property that contains console.log() 
// displaying text about self introduction about name, age, address and who is his/her friends.


// let person = {
// 	firstName: "Johnny",
// 	lastName: "Depp",
// 	nickName: "Jhon",
// 	age: 40,

// 	address: {
// 		city: 'Manila',
// 		country: 'Philippines'


// 	},
// 	friends:['Amber', 'James', 'Josh'],
// 	printDetails: function(){
// 		console.log("My name is " + person.firstName + " but you can call me " + person.nickName + " I am " + person.age + " I live in " + person.address.city + " , " + person.address.country + " I am with my friends " + person.friends);

// 	}
// }
// person.printDetails();


// Expected output:
// My Name is Juan Dela Cruz, but you can call me J. I am 25 years old and I live in Quezon City, Philippines. My friends are John, Jane, and Joe.

// Note: The ouput will be display through the function property inside the object variable. You may check about this keyword to solve the output.

// To enable screen reader support, press ⌘+Option+Z To learn about keyboard shortcuts, press ⌘slash
 


 // Create another copy of the "person" object use in the previous activity and apply the following ES6 updates:
 // 1. Apply the JavaScript Destructuring Assignment in the object variable.
 



// let person = {
// 	firstName: "Johnny",
// 	lastName: "Depp",
// 	nickName: "Jhon",
// 	age: 40,

// 	address: {
// 		city: 'Manila',
// 		country: 'Philippines'


// 	},

// 	friends:['Amber', 'James', 'Josh'],
	
// 	printDetails: function(){
// 	    //Destructuring
// 	    const { firstName, nickName, age, address, friends } = this;
// 	    const { city, country } = address;

// // 2. Use JavaScript Template Literals in displaying the text inside the function property of the object.
// 	    console.log(`My name is ${firstName} but you can call me ${nickName}. I am ${age} and I live in ${city}, ${country} , I am with my friends ${friends}`);
// 	}

// 	  //console.log("My name is " + firstName + " but you can call me " + nickName + " I am " + age + " I live in " + city + " , " + country + " I am wi
	
// }	 // 3. Run the program and check if it still produce the expected output.
// person.printDetails();

